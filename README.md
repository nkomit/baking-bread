## This is a bakery site template.
### Built on Node.js (Express) with MongoDB. Live: https://baking-bread.herokuapp.com/
### Demo:

<img src="/uploads/46a585b3423ac491cc09e0ad69b5a4d4/bakery_gitlab1.gif" width="600" height="450">
