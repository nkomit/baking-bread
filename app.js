const express = require('express');
const app = express();
require("dotenv").config();
const port = process.env.PORT || 8000;

const mongoose = require('mongoose');
router = express.Router();
const session = require('express-session');
const MongoStore = require('connect-mongo');

const pug = require('pug');
const path = require('path');
const bodyParser = require('body-parser');

//ROUTERS
const serverRouter = require('./routes/index');

///// DATABASE CONNECTION /////
const clientP = mongoose.connect(
  process.env.BAKERY_DB_URI,
  { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false },
  ).then(m => m.connection.getClient())

const db = mongoose.connection;

//middleware
app.use(session({
    secret: process.env.SECRET_KEY,
    resave: true,
    rolling: true,
    saveUninitialized: true,
    store: MongoStore.create({
      clientPromise: clientP,
      dbName: "bakery",
      stringify: false,
      autoRemove: 'interval',
      autoRemoveInterval: 10
    }),
    cookie: {
      maxAge: 180 * 60 * 1000, // 3 hours
      secure: false,
      sameSite: 'strict'
    }
}));

app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
});

app.use(express.json({ limit: '1mb' }));
app.use(bodyParser.json());
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/styles', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/sass', express.static(__dirname + '/node_modules/bootstrap/scss'));
app.use('/fonts', express.static(__dirname + '/node_modules/@fortawesome/fontawesome-free'));

app.use('/', serverRouter);

// Set pug as view engine
app.set('view engine', 'pug');
app.get('/', (req, res) => {
  req.session.foo = 'test-id';
  res.render('home');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
