const Product = require('../models/product');
const User = require('../models/user');

exports.getCart = async (req, res) => {
  let session_id = req.sessionID;
  let the_user = await User.findById(session_id);
  await the_user.populate({
    path: 'cart.item',
  }).execPopulate();
  res.render('cart', { title: 'Cart', the_user });
}

exports.addItems = async (req, res) => {
    try {
        let product_id = req.params.id;
        let product = await Product.findById(product_id);

        let session_id = req.sessionID;
        let the_user = await User.findById(session_id);

        //If quantity is less than 1, give an error
        if (req.body.quantity <= 0) {
          res.status(500).json({
              status: "error",
              message: "Please, pick at least one item",
              quantity: the_user.total.quantity
          })
        }
        else {
          // If the cart is empty - add an item. Else check, if such item is already in cart.
          // If it's not, add one. Else - increment its quantity.
          if (the_user.cart.length == 0) {
            the_user.cart.push({
              item: product,
              quantity: req.body.quantity
            })
          }
          else {
            function isIncart(x) {
              return x.item._id == product_id;
            }
            const exist = the_user.cart.find(isIncart);
            if (!exist) {
              console.log('item does not exist yet');
              the_user.cart.push({
                item: product,
                quantity: req.body.quantity
              })
            }
            else {
              console.log('the item is already in cart');
              console.log("exist quantity: " + exist.quantity);
              exist.quantity += req.body.quantity;
            }
          }

          console.log("req body quantity: " + req.body.quantity);

          product.left -= req.body.quantity;
          the_user.total.quantity += req.body.quantity;
          product.price = Number(product.price.toFixed(2))
          the_user.total.sum += (product.price * req.body.quantity);
          the_user.total.sum = Number(the_user.total.sum.toFixed(2));

          await the_user.populate({
            path: 'cart.item',
          }).execPopulate();

          if (product.left < 0) {
            the_user.total.quantity -= req.body.quantity;
            product.left += req.body.quantity;
            res.status(500).json({
                status: "error",
                message: "Sorry, there are " + product.left + " " + product.title + "s available! Please, pick another quantity or item",
                quantity: the_user.total.quantity
            })
          }
          else if (req.body.quantity == 1) {
            await product.save();
            await the_user.save();
            res.status(200).json({
              status: "success",
              message: product.title + " is added to cart!",
              quantity: the_user.total.quantity
            })
          }
          else {
            await product.save();
            await the_user.save();
            res.json({
              status: "success",
              message: req.body.quantity + " " + product.title + "s are added to cart!",
              quantity: the_user.total.quantity
            })
          }
        }
    } catch (err) {
        res.status(500).json({
            status: false,
            error: err
        })
    }
}

exports.deleteItems = async (req, res) => {
  try {
    let product_id = req.params.id;
    let session_id = req.sessionID;
    let the_user = await User.findById(session_id);
    // the deleted item
    let del_item = the_user.cart.find(function(e) {
      return e.id == product_id;
    })

    await the_user.populate({
      path: 'cart.item',
    }).execPopulate();

    the_user.cart = the_user.cart.filter(item => item != del_item);
    the_user.total.quantity -= del_item.quantity;
    the_user.total.sum -= (del_item.item.price * del_item.quantity);

    let product = await Product.findById(del_item.item.id);
    product.left += del_item.quantity;
    await product.save();

    await the_user.save();

    if (del_item.quantity == 1) {
      res.json({
        status: "success",
        message: del_item.item.title + " is deleted from cart!",
        quantity: del_item.quantity,
        total: Number(the_user.total.sum.toFixed())
      })
    }
    else {
      res.json({
        status: "success",
        message: del_item.quantity + " " + del_item.item.title + "s are deleted from cart!",
        quantity: del_item.quantity,
        total: Number(the_user.total.sum.toFixed())
      })
    }
  } catch (err) {
      res.status(500).json({
          status: false,
          error: err
      })
  }
}

exports.updateValue = async (req, res) => {
  try {
    let session_id = req.sessionID;
    let the_user = await User.findById(session_id);

    if (req.body.updated_qnt <= 0) {
      res.status(500).json({
          status: "error",
          message: "Please, pick at least one item",
          cart_quantity: the_user.total.quantity
      })
    }
    else {
      let item_id = req.params.id;
      let upd_item = the_user.cart.find(function(e) {
        return e.id == item_id;
      })

      await the_user.populate({
        path: 'cart.item',
      }).execPopulate();

      let product = await Product.findById(upd_item.item.id);

      async function countTotals() {
        upd_item.quantity = req.body.updated_qnt;
        let item_total = upd_item.quantity * upd_item.item.price;
        //new total sum
        the_user.total.sum = the_user.cart.reduce(function(a, b) {
          return a + b.item.price * b.quantity;
        }, 0)
        the_user.total.sum = Number(the_user.total.sum.toFixed(2));
        //new total quantity
        the_user.total.quantity = the_user.cart.reduce(function(a, b) {
          return a + b.quantity;
        }, 0)
        await the_user.save();
      }

      if (req.body.updated_qnt > upd_item.quantity) {
        if (product.left - (req.body.updated_qnt - upd_item.quantity) < 0) {
          res.status(500).json({
              status: "error",
              message: "Sorry, there are " + (product.left + upd_item.quantity) + " " + product.title + "s available! Please, pick another quantity or item",
              item_total: upd_item.quantity * upd_item.item.price,
              cart_total: the_user.total.sum,
              cart_quantity: the_user.total.quantity
          })
        }
        else {
          product.left -= req.body.updated_qnt - upd_item.quantity;
          await product.save();
          await countTotals();
          console.log("requested quantity is " + req.body.updated_qnt + "\n and quantity left is " + product.left);
          res.status(200).json({
            status: "success",
            message: "Quantity of " + product.title + 's has been changed to ' + req.body.updated_qnt,
            quantity: req.body.updated_qnt,
            item_total: Number((upd_item.quantity * upd_item.item.price).toFixed(2)),
            cart_total: the_user.total.sum,
            cart_quantity: the_user.total.quantity
          })
        }
      }
      else {
        product.left += upd_item.quantity - req.body.updated_qnt;
        await product.save();
        await countTotals();
        console.log("requested quantity is " + req.body.updated_qnt + "\n and quantity left is " + product.left);
        res.status(200).json({
          status: "success",
          message: "Quantity of " + product.title + 's has been changed to ' + req.body.updated_qnt,
          quantity: req.body.updated_qnt,
          item_total: Number((upd_item.quantity * upd_item.item.price).toFixed(2)),
          cart_total: the_user.total.sum,
          cart_quantity: the_user.total.quantity
        })
      }
    }
  } catch (err) {
      res.status(500).json({
          status: false,
          error: err
      })
  }
}

exports.refreshCart = async (req, res) => {
  try {
    let session_id = req.sessionID;
    let the_user = await User.findById(session_id);

    if (the_user.cart.length > 0) {
      the_user.active = false;
      await the_user.save();

      res.status(200).json({
        status: "Cart is empty",
        message: "Thank you! Your order is on the way 😀 Please wait for confirmation call from our manager",
        cart_qnt: 0
      })
    }
    else {
      res.status(500).json({
        status: "There are no items to delete!",
        message: "The cart is empty! You need to make an order first"
      })
    }
  } catch (err) {
      res.status(500).json({
          status: false,
          error: err
      })
  }
}
