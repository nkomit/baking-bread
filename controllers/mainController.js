const Product = require('../models/product');
const User = require('../models/user');

exports.getUser = async (req, res, next) => {
  let session_id = req.sessionID;
  let the_user;
  try {
    the_user = await User.findById(session_id);
    console.log("current user is: " + the_user.id);
    // if current user has ordered, or has been inactive for more than 3 hours, destroy current session
    if (the_user.active === false) {
      req.session.destroy();
      console.log('session is over');
    }
    else {
      console.log('session is still active');
    }
  } catch {
    // if there is no user - create
    console.log('the user does not exist');
    the_user = new User({
      _id: session_id,
      cart: []
    });
    await the_user.save();
    console.log("new user: " + the_user)
  } finally {
    next();
  }
}

exports.activityCheck = async (req, res, next) => {
  try {
    let current_date = new Date();

    // find active users updated 3 hours ago or earlier
    let users = await User.find({ updatedAt: { $lte: current_date - 180 * 60 * 1000 }, active: true });
    console.log("users updated 3 hours ago or earlier: " + users.length);

    // return products from abandoned carts to DB
    users.forEach(function(e) {
      let cart = e.cart;
      cart.forEach(function(e) {
        console.log('updating product...');
        Product.findByIdAndUpdate(e.item, { $inc: { left: e.quantity } }, function(err) {
          if (err) {
            throw err;
          }
        });
      })
      async function activeDisable() {
        e.active = false;
        await e.save();
        console.log('user is no longer active');
      }
      activeDisable();
      console.log(e);
    })
  } finally {
    next()
  }
}

exports.errorHandler = async (req, res) => {
  let the_user = await User.findById(req.sessionID);
  res.status(404).render('404', { the_user })
}


exports.index = async (req, res) => {
  let session_id = req.sessionID;
  let the_user = await User.findById(session_id);
  let products = await Product.find();
  res.render('home', { the_user, products })
}

exports.contacts = async (req, res) => {
  let session_id = req.sessionID;
  let the_user = await User.findById(session_id);
  res.render('contacts', { the_user })
}

exports.about = async (req, res) => {
  let session_id = req.sessionID;
  let the_user = await User.findById(session_id);
  res.render('about', { the_user })
}
