const nodemailer = require('nodemailer');
const multiparty = require('multiparty');

const Product = require('../models/product');
const User = require('../models/user');
const Order = require('../models/order');

// Nodemailer settings
let mailConfig = {
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
        user: process.env.USER,
        pass: process.env.PASS
    }
};
let transporter = nodemailer.createTransport(mailConfig);
transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

exports.checkout = async (req, res) => {
  try {
    // sending form
    let form = new multiparty.Form();
    let data = {};
    form.parse(req, function (err, fields) {
      console.log(fields);
      Object.keys(fields).forEach(function (property) {
        data[property] = fields[property].toString();
      });
    });

    let session_id = req.sessionID;
    let the_user = await User.findById(session_id);

    async function createOrder(data){
      const order = new Order({
        name: data.name,
        tel: data.phone,
        email: data.email,
        comment: data.comment,
        cart: the_user
      })
      await order.populate({
        path: 'cart.cart.item',
        select: 'title price -_id'
      }).execPopulate();
      await order.save();
      console.log(order.cart);
      return order;
    };

    if (the_user.cart.length > 0) {
      let order = await createOrder(data);
      const mail = {
        from: order.email,
        to: process.env.USER,
        text: `${order.name} \n<${order.email}> \n${order.tel} \n${order.comment} \n \n${order.cart.cart} \n \ntotal sum: ${order.cart.total.sum}`,
      };
      transporter.sendMail(mail, (err, data) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            message: "Something went wrong."
          });
        } else {
          console.log("Email is successfully sent!");
          res.status(200).json({
            message: "Email is successfully sent!",
            cart_qnt: the_user.total.quantity,
            cart_total: the_user.total.sum
          });
        }
      });
    }
    else {
      res.status(500).json({
        status: "Error! Email was not sent",
      })
    }
  } catch (err) {
    res.status(500).json({
        status: false,
        error: err
    })
  }
}
