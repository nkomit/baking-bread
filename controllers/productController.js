const Product = require('../models/product');
const User = require('../models/user');

exports.list = async (req, res) => {
  try {
    let products = await Product.find();
    console.log(req.query);
    if (Object.keys(req.query).length === 0) {
      console.log("query is empty");
    }
    else if (req.query.category) {
      products = await Product.find(req.query);
    }
    else {
      console.log('this query is for sort');
      products = await Product.find().sort(req.query);
    }
    let session_id = req.sessionID;
    let the_user = await User.findById(session_id);
    res.render('catalog', { title: 'Catalog', products, the_user })
  }
  catch (err) {
    res.status(500).json({
        status: false,
        error: err
    })
  }
}

exports.getProductByTitle = async (req, res) => {
  try {
      let title = req.params.title;
      let product = await Product.findOne({ title: [title] }).exec();
      let session_id = req.sessionID;
      let the_user = await User.findById(session_id);
      //total rating
      // reviews with non-null rates
      let rates_qnt = product.reviews.filter(elem => elem.rating > 0);
      product.rating = product.reviews.reduce(function(a, b) {
        return a + b.rating;
      }, 0)/rates_qnt.length;
      res.render('product', {title: title, product, the_user, rates_qnt});
  } catch (err) {
      res.status(500).json({
          status: false,
          error: err
      })
  }
}

exports.addReview = async (req, res) => {
  try {
    let user_id = req.sessionID;
    let title = req.params.title;
    let product = await Product.findOne({ title: [title] }).exec();
    let date = req.body.date;
    let avatar = req.body.avatar;

    console.log("comment date is: " + date);

    console.log(product.title + "\'s grade is " + req.body.grade);
    console.log(product.reviews);
    product.reviews.push({
      rating: req.body.grade,
      text: req.body.comment,
      userId: user_id,
      username: req.body.name,
      date: date,
      avatar: avatar
    })

    //total rating
    // reviews with non-null rates
    let rates_qnt = product.reviews.filter(elem => elem.rating > 0);

    if (rates_qnt.length == 0) {
      product.rating = 0;
    }
    else {
      product.rating = product.reviews.reduce(function(a, b) {
        return a + b.rating;
      }, 0)/rates_qnt.length;
    }

    console.log("new rating is " + product.rating);
    await product.save();

    res.status(200).json({
      status: "success",
      rating: req.body.grade,
      text: req.body.comment,
      userId: user_id,
      username: req.body.name,
      date: date
    })
  }
  catch (err) {
     res.status(500).json({
         status: false,
         error: err
     })
 }
}
