const mongoose = require('mongoose')
const schema = mongoose.Schema

const OrderSchema = new schema(
    {
      name: String,
      tel: String,
      email: String,
      comment: String,
      cart: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
      }
    }
)

module.exports = mongoose.model('Order', OrderSchema)
