const mongoose = require('mongoose')
const schema = mongoose.Schema

const ProductSchema = new schema({
  title: String,
  description: String,
  category: String,
  price: Number,
  left: Number,
  reviews: [
    {
      rating: Number,
      text: String,
      userId: String,
      username: String,
      date: String, 
      avatar: Number
    }
  ],
  rating: {
    type: Number,
    default: 0,
    required: true
  },
})

module.exports = mongoose.model('Product', ProductSchema)
