const mongoose = require('mongoose')
const schema = mongoose.Schema

const UserSchema = new schema(
    {
      _id: String,
      cart: [
        {
          item: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Product"
          },
          quantity: {
            type: Number,
            default: 1,
            required: true
          }
        },
      ],
      total: {
        quantity: {
          type: Number,
          default: 0,
          required: true
        },
        sum: {
          type: Number,
          default: 0,
          required: true
        }
      },
      active: {
        type: Boolean,
        default: true,
        required: true
      }
    },
    { timestamps: true }
)

module.exports = mongoose.model('User', UserSchema)
