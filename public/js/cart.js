const cart_qnt = document.getElementById("cart-qnt");
const cart_items = Array.from(document.getElementsByClassName('cart-item'));
const shopping_cart = document.getElementById('shopping-cart');
const modal_form = document.forms.modal_form;
const name = modal_form.querySelector('input[name=name]');
const phone = modal_form.querySelector('input[name=phone]');

cart_items.forEach(function(element) {
  //// remove item from cart
  let remove_btn = element.querySelector('.remove_btn');
  let item_total = element.querySelector('.item-total');
  let quantity = element.querySelector('input[type=number]');

  remove_btn.addEventListener('click', deleteFromCart);
  async function deleteFromCart(){
    try {
      const options = {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }
      let response = await fetch('/cart/' + remove_btn.name, options);
      console.log(response);
      let json = await response.json();
      console.log(json);
      cart_qnt.textContent = Number(cart_qnt.textContent) - json.quantity;
      document.getElementById('total').textContent = json.total;
      show_pop_msg();
      //popover message
      function show_pop_msg() {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
        document.querySelector('.message').style.display = "flex";
        document.querySelector('.toast-body').textContent = json.message;
        $('#shopping-cart').toast({ delay:5000 });
        $('#shopping-cart').toast('show');
      }
      element.remove();
    }
    catch(err) {
      console.log("oops, there is an error!");
      console.log(err);
    }
  }

  //// increment and decrement quantity
  quantity.addEventListener('input', updateValue);

  element.querySelector('.plus').addEventListener('click', function(){
    quantity.stepUp();
    updateValue();
  });
  element.querySelector('.minus').addEventListener('click', function() {
    quantity.stepDown();
    updateValue();
  });

  async function updateValue() {
    try {
      let data = { updated_qnt: Number(quantity.value) };
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
      let response = await fetch('/item/' + quantity.name, options);
      console.log(response);
      let json = await response.json();
      console.log(json);
      item_total.textContent = json.item_total;
      document.getElementById('total').textContent = json.cart_total;
      cart_qnt.textContent = json.cart_quantity;
      show_pop_msg();
      //popover message
      function show_pop_msg() {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
        if (response.status === 500) {
          document.querySelector('.message').classList.add('error-msg');
        }
        else {
          document.querySelector('.message').classList.remove('error-msg');
        }
        document.querySelector('.message').style.display = "flex";
        document.querySelector('.toast-body').textContent = json.message;
        $('#shopping-cart').toast({ delay:5000 });
        $('#shopping-cart').toast('show');
      }
    }
    catch(err) {
      console.log("oops, there is an error!");
      console.log(err);
    }
  }
})

/////FORM VALIDATION/////

/// NAME VALIDATION ///
name.addEventListener('input', () => {
  name.setCustomValidity('');
  name.checkValidity();
});

name.addEventListener('invalid', () => {
  if (name.value.length < 3) {
    name.setCustomValidity('Please enter your name')
  }
  else {
    name.setCustomValidity('Please use only letters')
  }
})
/// PHONE VALIDATION ///
phone.addEventListener('input', () => {
  phone.setCustomValidity('');
  phone.checkValidity();
});

phone.addEventListener('invalid', () => {
  if (phone.value === '') {
    phone.setCustomValidity('Please enter your contact number')
  }
  else {
    phone.setCustomValidity('Please use federal format: +7 or 8, then your number')
  }
})

/// CHECKOUT ///
modal_form.addEventListener('submit', (event) => {
  event.preventDefault();
  // shows success message
  $('#checkout .alert').show();
  let mail = new FormData(modal_form);
  sendMail(mail);
  console.log(mail);
  console.log('success!');
  modal_form.reset();
  refreshCart();
  $("#checkout").on("hidden.bs.modal", function () {
    $('#checkout .alert').hide();
  });
})

async function sendMail(mail) {
  try {
    const options = {
      method: 'POST',
      body: mail
    }
    let response = await fetch('/checkout', options);
    console.log(response);
    let json = await response.json();
    console.log(json);
  }
  catch(err) {
    console.log("oops, there is an error!");
    console.log(err);
  }
}

async function refreshCart() {
  try {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    let response = await fetch('/cart', options);
    console.log(response);
    let json = await response.json();
    console.log(json);
    cart_items.forEach((e) => {
      e.remove();
    })
    $('#cart-list .alert').remove();
    cart_qnt.textContent = json.cart_qnt;
    if (response.status == 200) {
      document.querySelector('#checkout .alert').classList.add('alert-success');
    }
    else {
      document.querySelector('#checkout .alert').classList.add('alert-danger');
    }
    document.querySelector('#checkout .alert p').textContent = json.message;
  }
  catch(err) {
    console.log("oops, there is an error!");
    console.log(err);
  }
}
