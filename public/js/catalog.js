const add_to_cart = Array.from(document.getElementsByClassName("add-to-cart"));
const cart_qnt = document.getElementById("cart-qnt");
const shopping_cart = document.getElementById("shopping-cart");

add_to_cart.forEach(function(element){
  // submit event listener
  let data = { quantity: 1 };
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }

  element.addEventListener('submit', addToCart);

  async function addToCart(event) {
    event.preventDefault();
    try {
      let response = await fetch('/cart/' + element[0].name, options);
      console.log(response);
      let json = await response.json();
      console.log(json);
      cart_qnt.textContent = json.quantity;
      show_pop_msg();
      //popover message
      function show_pop_msg() {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
        if (response.status === 500) {
          document.querySelector('.message').classList.add('error-msg');
        }
        else {
          document.querySelector('.message').classList.remove('error-msg');
        }
        document.querySelector('.message').style.display = "flex";
        document.querySelector('.toast-body').textContent = json.message;
        $('#shopping-cart').toast({ delay:5000 });
        $('#shopping-cart').toast('show');
      }
    }
    catch(err) {
      console.log("oops, there is an error!");
      console.log(err);
    }
  }
})

/// DROPDOWNS
$(document).ready(function(){
    // Show hide popover
    $(".dropdown").click(function(){
        $(this).find(".dropdown-menu").slideToggle("fast");
    });
});
$(document).on("click", function(event){
    let $trigger = $(".dropdown");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".dropdown-menu").slideUp("fast");
    }
});

/// filter by category
Array.from(document.querySelector('[aria-labelledby=category]').childNodes).forEach(function(elem) {
  elem.addEventListener("click", filterByCategory);
  function filterByCategory() {
    document.location.search = "category=" + elem.textContent;
  }
})

/// sort by price and rating
Array.from(document.querySelector('[aria-labelledby=sortby]').childNodes).forEach(function(elem) {
  elem.addEventListener("click", sortBy);
  function sortBy() {
    if (elem.textContent == "Rating ↓") {
      document.location.search = "rating=desc";
    }
    else if (elem.textContent == "Rating ↑") {
      document.location.search = "rating=asc";
    }
    else if (elem.textContent == "Price ↓") {
      document.location.search = "price=desc";
    }
    else if (elem.textContent == "Price ↑") {
      document.location.search = "price=asc";
    }
  }
})
