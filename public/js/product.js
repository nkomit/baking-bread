//Shopping cart selectors
const cart_qnt = document.getElementById("cart-qnt");
const shopping_cart = document.getElementById("shopping-cart");
//FORMS
const add_form = document.forms.add_form;
const reviews = document.forms.reviews;
//REVIEW form
const stars = Array.from(document.querySelectorAll('.stars label'));
const username = reviews.querySelector('input[name=name]');
const comment = document.querySelector('textarea');
let grade = 0;
const comment_section = document.querySelector('.comment-section');
//Add-to-cart form
const quantity = document.querySelector('input[type=number]');
//buttons
let active_btns = Array.from(document.querySelectorAll('.btns'));

/// Adding products to cart
if (add_form){
  /// submit event listener
  add_form.addEventListener('submit', addToCart);

  async function addToCart(event) {
    event.preventDefault();
    try {
      let data = { quantity: Number(quantity.value) };
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
      let response = await fetch('/cart/' + add_form[0].name, options);
      console.log(response);
      let json = await response.json();
      console.log(json);
      cart_qnt.textContent = json.quantity;
      show_pop_msg();
      //popover message
      function show_pop_msg() {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
        if (response.status === 500) {
          document.querySelector('.message').classList.add('error-msg');
        }
        else {
          document.querySelector('.message').classList.remove('error-msg');
        }
        document.querySelector('.message').style.display = "flex";
        document.querySelector('.toast-body').textContent = json.message;
        $('#shopping-cart').toast({ delay:5000 });
        $('#shopping-cart').toast('show');
      }
    }
    catch(err) {
      console.log("oops, there is an error!");
      console.log(err);
    }
  }

  //increment and decrement quantity
  document.querySelector('.plus')
  .addEventListener('click', function(event) {
      quantity.stepUp();
  })

  document.querySelector('.minus')
  .addEventListener('click', function(event) {
      quantity.stepDown();
  })
}

reviews.addEventListener('submit', addReview);

async function addReview(event) {
  event.preventDefault();
  try {
    let date = new Date();
    date = date.toLocaleDateString('en-GB', {year: 'numeric', month: 'long', day: 'numeric'})
    + " " + date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    let data = {
      name: username.value,
      comment: comment.value,
      grade: Number(grade),
      date: date,
      avatar: Math.floor(Math.random()*4)
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
    let response = await fetch('/catalog/' + reviews.title, options);
    let json = await response.json();

    // if this is the 1st comment, remove "no comments yet"
    if (comment_section.querySelector('.comment-default')) {
      comment_section.querySelector('.comment-default').remove();
    }

    // create and add a comment to the comment section
    let new_comment = document.createElement('div');
    new_comment.classList.add('comment-body');

    let avatar = document.createElement('img');
    avatar.src = "/images/avatars/" + data.avatar + ".jpg";
    avatar.classList.add('avatar');
    let ul = document.createElement('ul');
    let first_li = document.createElement('li');
    let name = document.createElement('p');
    name.classList.add('comment-name');
    name.textContent = json.username;
    let comm_date = document.createElement('p');
    comm_date.classList.add('comment-date', 'text-muted');
    comm_date.textContent = json.date;
    first_li.append(name, comm_date)
    // if there is a rating, display stars
    let second_li = document.createElement('li');
    let comment_stars = document.createElement('div');
    if (grade > 0) {
      let n = 0;
      comment_stars.classList.add('stars');
      while (n < json.rating){
        ++n;
        comment_stars.appendChild(document.createElement('i')).classList.add('far','fa-star');
      }
      second_li.append(comment_stars);
    }
    let third_li = document.createElement('li');
    let text = document.createElement('p');
    text.classList.add('comment-text');
    text.textContent = json.text;
    third_li.append(text);

    ul.append(first_li, second_li, third_li);
    new_comment.append(avatar, ul);
    comment_section.append(new_comment);

    // reset stars and reviews form
    for (i in stars) {
      stars[i].classList.remove('rated');
    }
    reviews.reset();
  }
  catch(err) {
    console.log("oops, there is an error!");
    console.log(err);
  }
}

/// RATING in review section
stars.forEach(function(elem) {
  elem.addEventListener("click", function(event) {
    grade = elem.htmlFor;
    for (i in stars) {
      if ((stars[i].htmlFor) <= grade) {
        stars[i].classList.add('rated');
      }
      else {
        stars[i].classList.remove('rated');
      }
    }
  });
})

/// on hover show larger avatars
Array.from(document.getElementsByClassName('avatar')).forEach(function(e){
  e.addEventListener('mouseover', function(){
    e.closest('.comment-body').querySelector('.full-size').classList.add('visible')
  })
  e.addEventListener('mouseout', function(){
    e.closest('.comment-body').querySelector('.full-size').classList.remove('visible')
  })
})
