const express = require('express');
const router = express.Router();

// Require controller modules.
const main_controller = require('../controllers/mainController');
const product_controller = require('../controllers/productController');
const cart_controller = require('../controllers/cartController');
const order_controller = require('../controllers/orderController');

router.all('*', main_controller.getUser);
router.all('*', main_controller.activityCheck);

router.post('/checkout', order_controller.checkout);
router.get('/', main_controller.index);
router.get('/contacts', main_controller.contacts);
router.get('/about', main_controller.about);
router.get('/catalog', product_controller.list);
router.get('/catalog/:title', product_controller.getProductByTitle);
router.post('/catalog/:title', product_controller.addReview);

router.get('/cart', cart_controller.getCart);
router.post('/cart/:id', cart_controller.addItems);
router.delete('/cart/:id', cart_controller.deleteItems);

router.post('/item/:id', cart_controller.updateValue);
router.post('/cart', cart_controller.refreshCart);

router.use(main_controller.errorHandler);

module.exports = router;
